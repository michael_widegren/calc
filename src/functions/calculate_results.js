const calculateResults = (input) => {

  // Cred to https://medium.com/@stoopidguy1992/how-to-write-a-math-expression-parser-in-javascript-b5147bc9466b

  // this will only take strings containing * operator [ no + ]
  const parseMultiplicationSeparatedExpression = (expression) => {
    const numbersString = expression.split("*");
    const numbers = numbersString.map((noStr) => +noStr);
    const initialValue = 1.0;
    const result = numbers.reduce((acc, no) => acc * no, initialValue);
    return result;
  };

  // both * -
  const parseMinusSeparatedExpression = (expression) => {
    const numbersString = expression.split("-");
    const numbers = numbersString.map((noStr) =>
      parseMultiplicationSeparatedExpression(noStr)
    );
    const initialValue = numbers[0];
    const result = numbers.slice(1).reduce((acc, no) => acc - no, initialValue);
    return result;
  };

  // * - +
  const parsePlusSeparatedExpression = (expression) => {
    const numbersString = expression.split("+");
    const numbers = numbersString.map((noStr) =>
      parseMinusSeparatedExpression(noStr)
    );
    const initialValue = 0.0;
    const result = numbers.reduce((acc, no) => acc + no, initialValue);
    return result;
  };

  return parsePlusSeparatedExpression(input);

};

export default calculateResults;
