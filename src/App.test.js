import calc from './functions/calculate_results';

test('calculate expression', () => {
  expect(calc('7-5+3*11')).toBe(35);
});
