/* for testing on localhost */

const express = require("express");
const cors = require('cors');
const app = express();
const port = 3000;
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Server");
});

app.post("/calc", (req, res) => {
  const result = calculateResults(req.body.input);
  res.send(JSON.stringify({ result }));
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

const calculateResults = (input) => {
  // this will only take strings containing * operator [ no + ]
  const parseMultiplicationSeparatedExpression = (expression) => {
    const numbersString = expression.split("*");
    const numbers = numbersString.map((noStr) => +noStr);
    const initialValue = 1.0;
    const result = numbers.reduce((acc, no) => acc * no, initialValue);
    return result;
  };

  // both * -
  const parseMinusSeparatedExpression = (expression) => {
    const numbersString = expression.split("-");
    const numbers = numbersString.map((noStr) =>
      parseMultiplicationSeparatedExpression(noStr)
    );
    const initialValue = numbers[0];
    const result = numbers.slice(1).reduce((acc, no) => acc - no, initialValue);
    return result;
  };

  // * - +
  const parsePlusSeparatedExpression = (expression) => {
    const numbersString = expression.split("+");
    const numbers = numbersString.map((noStr) =>
      parseMinusSeparatedExpression(noStr)
    );
    const initialValue = 0.0;
    const result = numbers.reduce((acc, no) => acc + no, initialValue);
    return result;
  };

  return parsePlusSeparatedExpression(input);
};


/* 
Example

fetch("http://localhost:3000/calc", {
  method: "POST",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  body: JSON.stringify({ input: "2+2+2*8" }),
})
  .then((response) => response.json())
  .then((result) => {
    console.log("Success:", result);
  })
  .catch((error) => {
    console.error("Error:", error);
  });

*/