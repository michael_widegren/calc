import { useState } from "react";
import calculateResults from "../functions/calculate_results";
import remove from "../functions/remove";

const Calculator = () => {
  const [state, setState] = useState("");

  const DIGITS = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0].map((item, i) => (
    <button key={i} onClick={(e) => handleClickNumpad(e)} value={item}>
      {item}
    </button>
  ));

  const OPERATORS = ["+", "-", "*"].map((item, i) => (
    <button key={i} onClick={(e) => handleClickNumpad(e)} value={item}>
      {item}
    </button>
  ));

  const handleClickNumpad = (e) => {
    const VAL = e.target.value;
    setState(state + VAL);
  };

  const calculateAsPostRequest = (input) => {

    fetch("http://localhost:3000/calc", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ input }),
    })
      .then((response) => response.json())
      .then((result) => {
        console.log("Success:", result);
      })
      .catch((error) => {
        console.error("Error:", error);
        console.log("POST requiers local server setup");
      });
  };

  return (
    <section className="calculator">
      <div className="display">
        <div className="display__history">
          {state.split(/[+\-*]/).length > 1 && state}
        </div>
        <div className="display__results">
          {state.slice(-1) === "*" ? "" : calculateResults(state)}
        </div>
   
      </div>
      <div className="numpad">
        <div className="numpad__digits">{DIGITS}</div>
        <div className="numpad__operators">
          <button onClick={() => setState("")}>C</button>
          <button onClick={() => setState(remove(state))}>&#x232B;</button>
          {OPERATORS}
        </div>
      </div>
      <button
      className="post" title="Requiers local server setup"
      onClick={() => calculateAsPostRequest(state)}
    >(POST)</button>
    </section>
  );
};

export default Calculator;
