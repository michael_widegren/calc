const remove = (expression) => {
  if (!expression.length) return "";

  return expression.slice(0, -1);
};

export default remove;
